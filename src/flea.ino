// Flea
// Water-rocket altimeter and recovery-release system
// By Greg Fawcett <greg@vig.co.nz>
// This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.


#include <Servo.h>
#include <Wire.h>
#include <libBmp180.h>

// How long to show a character, in microseconds
#define DISPLAY_DELAY 1000000
// How long to blank between characters in microseconds
#define DISPLAY_BLANK 100000
// How long between pressure readings, in microseconds
// Update DT below if changed.
#define SAMPLE_DELAY 62500

Servo recovery_servo;	// Parachute release servo
Bmp180 sensor;	// Altitude sensor

char display_buffer[20];
char *display_pos;
char display_blank;
unsigned long now, time_to_blank, time_for_next_char, time_for_next_sample;

double sh; // Sample height

// Kalman calculation variables
// h:height v:velocity
// u:updated c:updated and corrected
// Assume acceleration is constant -9.8m/s/s so don't need ca, ua or KA
double ch, cv, uh, uv;
#define KH 0.2 // Kalman gain height
#define KV 0.5 // Kalman gain velocity
#define DT 0.0625
// Save some long multiplication using these constants
#define DH_G -0.01914 // The change in height due to gravity in 0.0625s (g*dt*dt/2)
#define DV_G -0.6125 // The change in velocity due to gravity in 0.0625s (g*dt)

// Ground level exponential moving average variables
double agl; // Average ground level
double emafactor=0.1; // Exponetial moving average factor for AGL

// Status variable
#define STATUS_GROUND 0
#define STATUS_LAUNCH 1
#define STATUS_APOGEE 2
char status;

// Which segments to turn on for a given character.
// Assumes display top is connected to pins 6-9 and bottom to pins 10-13.
#define CHAR_POINT  10
#define CHAR_WAIT1  11
#define CHAR_WAIT2  12
#define CHAR_WAIT3  13
#define CHAR_LAUNCH 14
#define CHAR_SPACE  15
#define CHAR_ERROR  16

unsigned char char_bits[]=
{		  // i  ch
	0x77, // 0  0
	0x41, // 1  1
	0x3b, // 2  2
	0x6b, // 3  3
	0x4d, // 4  4
	0x6e, // 5  5
	0x7e, // 6  6
	0x47, // 7  7
	0x7f, // 8  8
	0x6f, // 9  9
	0x80, // 10 .
	0x20, // 11 a (wait1)
	0x08, // 12 b (wait2)
	0x02, // 13 c (wait3)
	0x34, // 14 L
	0x00, // 15 Space
	0x3e  // 16 Error
};

//----------------------------------------------------------------------
// Set the correct outputs to display the given character.
void display_char(char c)
{
	char ci;
	if(isdigit(c))
		ci=atoi(&c);
	else if(c=='.')
		ci=CHAR_POINT;
	else if(c=='a')
		ci=CHAR_WAIT1;
	else if(c=='b')
		ci=CHAR_WAIT2;
	else if(c=='c')
		ci=CHAR_WAIT3;
	else if(c=='L')
		ci=CHAR_LAUNCH;
	else if(c==' ')
		ci=CHAR_SPACE;
	else
		ci=CHAR_ERROR;
	for(char i=0; i<8; ++i)
		digitalWrite(6+i, char_bits[ci]&(1<<i)); // Common cathode display
//		digitalWrite(6+i, ~char_bits[ci]&(1<<i)); // Common anode display - invert
}

int count=0;
int start=0;

//----------------------------------------------------------------------
void setup()
{
	Serial.begin(9600);
	while(!Serial);
	Serial.println("*** RESET ***");

	// Initialise display pins
	pinMode(6, OUTPUT);
	pinMode(7, OUTPUT);
	pinMode(8, OUTPUT);
	pinMode(9, OUTPUT);
	pinMode(10, OUTPUT);
	pinMode(11, OUTPUT);
	pinMode(12, OUTPUT);
	pinMode(13, OUTPUT);

	// Initialise sensor
	display_char('8'); // Display "B" for BMP180
	uint8_t init_result=sensor.init(BMP180_STANDARD);
	agl=sensor.altitude(sensor.pressure()); // Initial reading for average ground level

	// Cycle servo
	display_char('5'); // Display "S" for servo
	recovery_servo.attach(3); // attaches the servo on pin 9 to the servo object
	recovery_servo.write(90); // tell servo to go to open position
	delay(5000);
	recovery_servo.write(0);	 // tell servo to go to closed position

	// Display wait characters
	strcpy(display_buffer, "abc");
	display_pos=display_buffer;
	time_for_next_char=micros(); // Show first char immediately

	// Initialise status
	status=STATUS_GROUND;
}

//----------------------------------------------------------------------
void loop()
{

	now=micros();

	// Check if time to display next character
	if(now>time_for_next_char)
	{
		display_char(*display_pos++);
		display_blank=0;
		if(!*display_pos) // If end of display_buffer, go back to start.
			display_pos=display_buffer;
		time_for_next_char+=DISPLAY_DELAY;
	}
	// Check if time to blank display
	else if(!display_blank && now>time_for_next_char-DISPLAY_BLANK)
	{
		display_char(' ');
		display_blank=1;
	}

	// Check if time for next sample
	if(status<STATUS_APOGEE && now>time_for_next_sample)
	{
		sh=sensor.altitude(sensor.pressure());
		time_for_next_sample+=SAMPLE_DELAY;
		if(status==STATUS_GROUND) // Waiting for launch
		{
			if(sh>agl+2.0) // 2m is over 6 sigmas (1.8m) so false launch detect once in several years
			{
				++status; // Change status to STATUS_LAUNCH
				ch=sh; // First estimate for height
				cv=(sh-agl)/DT; // First estimate for velocity
				strcpy(display_buffer, "L");
				display_pos=display_buffer;
				time_for_next_char=micros(); // Show first char immediately
			}
			else
				agl=emafactor*sh+(1-emafactor)*agl;
		}
		else // Launched
		{
			if(cv<0) // Corrected velocity below zero - just passed apogee
			{
				++status; // Change status to STATUS_APOGEE
				// Arduino sprintf can't cope with floating point, so we turn altitude into two integers
				uint16_t a_dm=round((ch-agl)*10.0); // Rounded altitude in decimeters
				sprintf(display_buffer, "%d.%d ", a_dm/10, a_dm%10);
				display_pos=display_buffer;
				time_for_next_char=now; // Show height immediately
				recovery_servo.write(90); // tell servo to go to open position

			}
			else // Not at apogee yet - update Kalman filter
			{
				// Kalman state update: use last corrected h, v, a to calculate expected h, v, a
				uh=ch+cv*DT+DH_G;
				uv=cv+DV_G;
				// Kalman state correction: use new reading to correct expected state
				ch= uh+KH*(sh-uh);
				cv=uv+KV*(sh-uh);
			}
		}
	}
}

