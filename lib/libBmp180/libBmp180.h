#ifndef libBmp180_h
#define libBmp180_h

// libBmp180.h - Library for BMP180 Sensor.
// By Greg Fawcett <greg@vig.co.nz>
// This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.

#include <Wire.h>
#include <Arduino.h>

#define LIBBMP180_DEBUG 0

// Accuracy modes
#define BMP180_ULTRALOWPOWER	0
#define BMP180_STANDARD			1
#define BMP180_HIGHRES			2
#define BMP180_ULTRAHIGHRES		3

#define BMP180_ADDRESS			0x77
#define BMP180_REQ_ID			0xD0
#define BMP180_CHIP_ID			0x55

// Calibration data addresses
#define BMP180_AC1_ADDR			0xAA
#define BMP180_AC2_ADDR			0xAC
#define BMP180_AC3_ADDR			0xAE
#define BMP180_AC4_ADDR			0xB0
#define BMP180_AC5_ADDR			0xB2
#define BMP180_AC6_ADDR			0xB4
#define BMP180_B1_ADDR			0xB6
#define BMP180_B2_ADDR			0xB8
#define BMP180_MB_ADDR			0xBA
#define BMP180_MC_ADDR			0xBC
#define BMP180_MD_ADDR			0xBE

#define BMP180_CONTROL			0xF4
#define BMP180_DATA				0xF6
#define BMP180_READTEMPCMD		0x2E
#define BMP180_READPRESSURECMD	0x34

// BMP180 Pressure Sensor class
class Bmp180
{

    public:
	// Constructor
	Bmp180(void);
	~Bmp180(void);
	// Set up comms to sensor and set accuracy.
	// Returns 0 if OK or error code.
	uint8_t init(uint8_t accuracy);
	// Get altitude in metres
	double altitude(int32_t pressure);
	// Get pressure in pascals
	int32_t pressure(void);
	// Get temperature in celcius
	double temperature(void);

    private:
	uint8_t _oss;
	int16_t _ac1, _ac2, _ac3, _b1, _b2, _mb, _mc, _md;
	uint16_t _ac4, _ac5, _ac6;

	uint8_t _pra_cnt, _pra_idx;
	uint32_t _pra_sum;

	int32_t _getB5(void);
	uint32_t _read(uint8_t addr, uint8_t bytes);
	void _write(uint8_t addr, uint8_t data);
	void _pra_clear(void);
	void _pra_add(uint32_t value);
	uint32_t _pra_average();

};

#endif
