// libBmp180.cpp - Library for BMP180 Sensor.
// By Greg Fawcett <greg@vig.co.nz>
// This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.

#include <libBmp180.h>

const int read_pressure_delay[] = {5, 8, 15, 28};

//----------------------------------------------------------------------
// BMP180 Pressure Sensor class

//----------------------------------------------------------------------
// Constructor
Bmp180::Bmp180(void)
{
}

//----------------------------------------------------------------------
Bmp180::~Bmp180()
{
}

//----------------------------------------------------------------------
// Initialise BMP180
uint8_t Bmp180::init(uint8_t accuracy)
{
	_oss=accuracy;
	Wire.begin();
	if(_read(BMP180_REQ_ID, 1)!=BMP180_CHIP_ID)
		return 1; // Bad chip ID
	_ac1=_read(BMP180_AC1_ADDR, 2);
	_ac2=_read(BMP180_AC2_ADDR, 2);
	_ac3=_read(BMP180_AC3_ADDR, 2);
	_ac4=_read(BMP180_AC4_ADDR, 2);
	_ac5=_read(BMP180_AC5_ADDR, 2);
	_ac6=_read(BMP180_AC6_ADDR, 2);
	_b1=_read(BMP180_B1_ADDR, 2);
	_b2=_read(BMP180_B2_ADDR, 2);
	_mb=_read(BMP180_MB_ADDR, 2);
	_mc=_read(BMP180_MC_ADDR, 2);
	_md=_read(BMP180_MD_ADDR, 2);

#if (LIBBMP180_DEBUG==1)
	Serial.print("ac1:"); Serial.println(_ac1);
	Serial.print("ac2:"); Serial.println(_ac2);
	Serial.print("ac3:"); Serial.println(_ac3);
	Serial.print("ac4:"); Serial.println(_ac4);
	Serial.print("ac5:"); Serial.println(_ac5);
	Serial.print("ac6:"); Serial.println(_ac6);
	Serial.print("b1:"); Serial.println(_b1);
	Serial.print("b2:"); Serial.println(_b2);
	Serial.print("mb:"); Serial.println(_mb);
	Serial.print("mc:"); Serial.println(_mc);
	Serial.print("md:"); Serial.println(_md);
#endif

	return 0; // All good
}

//----------------------------------------------------------------------
// Get altitude in metres
// Sealevel pressure: 101325 Pascals
double Bmp180::altitude(int32_t pressure)
{
	return 44330*(1.0-pow(pressure/(double)101325, 0.1903));
}

//----------------------------------------------------------------------
// Get pressure in Pascals
int32_t Bmp180::pressure()
{
	int32_t up, b3, b6, x1, x2, x3, p;
	uint32_t b4, b7, data;

	_write(BMP180_CONTROL, BMP180_READPRESSURECMD+(_oss<<6));
	delay(read_pressure_delay[_oss]);
	data=_read(BMP180_DATA, 3);
	up=data>>(8-_oss);
	b6=_getB5()-4000;
	x1=((int32_t)_b2*((b6*b6)>>12))>>11;
	x2=((int32_t)_ac2*b6)>>11;
	x3=x1+x2;
	b3=((((int32_t)_ac1*4+x3)<<_oss)+2)>>2;
	x1=((int32_t)_ac3*b6)>>13;
	x2=((int32_t)_b1*((b6*b6)>>12))>>16;
	x3=((x1+x2)+2)>>2;
	b4=((uint32_t)_ac4*(uint32_t)(x3+32768))>>15;
	b7=((uint32_t)up-b3)*(uint32_t)(50000ul>>_oss);
	if(b7<0x80000000)
		p=(b7*2)/b4;
	else
		p=(b7/b4)*2;
	x1=(p>>8)*(p>>8);
	x1=(x1*3038)>>16;
	x2=(-7357*p)>>16;
	p=p+((x1+x2+(int32_t)3791)>>4);
	return p;
}

//----------------------------------------------------------------------
// Get temperature in celcius
double Bmp180::temperature(void)
{
	return (_getB5()+8)/160.0;
}

//----------------------------------------------------------------------
// Get B5 temperature factor (for getPressure() amd getTemperature())
int32_t Bmp180::_getB5(void)
{
	int32_t ut, x1, x2;

	_write(BMP180_CONTROL, BMP180_READTEMPCMD);
	delay(5);					// Datasheet suggests 4.5 ms
	ut=_read(BMP180_DATA, 2);
	x1=(ut-(int32_t)_ac6)*(int32_t)_ac5>>15;
	x2=((int32_t)_mc<<11)/(x1+(int32_t)_md);
#if (LIBBMP180_DEBUG==1)
	Serial.println("--------");
	Serial.print("ut:"); Serial.println(ut);
	Serial.print("ac6:"); Serial.println(_ac6);
	Serial.print("ac5:"); Serial.println(_ac5);
	Serial.print("x1:"); Serial.println(x1);
	Serial.print("x2:"); Serial.println(x2);
	Serial.print("b5:"); Serial.println(x1+x2);
#endif
	return x1+x2;
}

//----------------------------------------------------------------------
// Read up to 4 bytes from an address
uint32_t Bmp180::_read(uint8_t addr, uint8_t count)
{
	uint32_t val;
	uint8_t data;

	Wire.beginTransmission(BMP180_ADDRESS);
	Wire.write(addr);
	Wire.endTransmission();
	Wire.beginTransmission(BMP180_ADDRESS);
	Wire.requestFrom((uint8_t)BMP180_ADDRESS, count);
	while(Wire.available()!=count); // wait until count bytes are available
	val=0;
	while(count--)
	{
		data=Wire.read();
//		Serial.print("read "); Serial.println(data, HEX);
		val=(val<<8)|data;
	}
	Wire.endTransmission();
	return val;
}

//----------------------------------------------------------------------
// Write a byte of data to an address
void Bmp180::_write(uint8_t addr, uint8_t data)
{
	Wire.beginTransmission(BMP180_ADDRESS);
	Wire.write(addr);
	Wire.write(data);
	Wire.endTransmission();
//	Serial.print("write "); Serial.print(data, HEX); Serial.print(" to "); Serial.println(addr, HEX);
}
